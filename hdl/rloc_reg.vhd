----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/19/2022 11:44:40 PM
-- Design Name: 
-- Module Name: rloc_reg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description:
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
-- use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rloc_reg is
    generic(
        set_name    	: string;
        rloc_index_y  	: integer;
        rloc_index_x  	: integer
        );
    port(
        clk : in std_logic;
        p   : in std_logic;
        q   : out std_logic := '0'
        );
end rloc_reg;

architecture Behavioral of rloc_reg is
    
    --RLOC attributes
    attribute U_SET : string;
    attribute RLOC  : string;
    
    attribute U_SET of q  : signal is set_name;
    attribute RLOC of q   : signal is "X" & integer'IMAGE(rloc_index_x) & "Y" & integer'IMAGE(rloc_index_y);

begin

    p_ff : process(clk)
    begin
        if rising_edge(clk) then
            q <= p;
        end if;
    end process p_ff;


end Behavioral;
