----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/21/2022 09:16:14 PM
-- Design Name: 
-- Module Name: wfc_adder_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library ecc;
use ecc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity wfc_adder_tb is
--  Port ( );
end wfc_adder_tb;

architecture Behavioral of wfc_adder_tb is

    constant period : time := 10 ns;
    constant width : integer := 48*2;
    constant K_div2 : integer := 8*2;

    signal sys_clk : std_logic := '0';
    
    signal adder_comb   : std_logic_vector(2*width-1 downto 0) := X"FFFF_FFFF_FFFF" & X"FFFF_FFFF_FFF0" & X"FFFF_FFFF_FFFF" & X"FFFF_FFFF_FFF0";
    --signal adder_comb   : std_logic_vector(2*width-1 downto 0) := (others => '0');
    signal lfsr_data    : std_logic_vector(31 downto 0) := (others => '0');
    signal adder_in_a   : std_logic_vector(width-1 downto 0) := (others => '0');
    signal adder_in_b   : std_logic_vector(width-1 downto 0) := (others => '0');
    signal adder_in_a_d1   : std_logic_vector(width-1 downto 0) := (others => '0');
    signal adder_in_b_d1   : std_logic_vector(width-1 downto 0) := (others => '0');
    signal adder_out    : std_logic_vector(width downto 0) := (others => '0');
    signal adder_out_glob    : std_logic_vector(width downto 0) := (others => '0');
    
    signal adder_validate : std_logic_vector(width downto 0) := (others => '0');
    signal adder_validate_d1 : std_logic_vector(width downto 0) := (others => '0');

    constant ones : std_logic_vector(width-2 downto 0) := (others => '1');
    constant zeros : std_logic_vector(width-2 downto 0) := (others => '0');

begin

    sys_clk <= not sys_clk after period/2;
    
    adder_in_a <= adder_comb(width-1 downto 0);
    adder_in_b <= adder_comb(2*width-1 downto width);
    
    
    LFSR_inst : entity ecc.LFSR
        generic map(
            width => 32,
            init => X"F0F1F0F1")
        port map(
            clk => sys_clk,
            lfsr_data => lfsr_data
            );
    
--    wfc_adder_inst : entity ecc.wfc_adder
--        generic map(
--                adder_name => "my_wfc",
--                width => width,
--                K_div2 => K_div2)
--        port map(
--             clk => sys_clk,
--             a => adder_in_a,
--             b => adder_in_b,
--             c_in => '0',
--             --output => adder_out
--             output => adder_out(width-1 downto 0),
--             c_out => open
--             );

    wfc_adder_inst : entity ecc.wfc_p_adder
        generic map(
                adder_name => "my_wfc",
                width => width,
                K_div2 => K_div2)
        port map(
             clk => sys_clk,
             a => adder_in_a,
             b => adder_in_b,
             c_in => '0',
             --output => adder_out
             output => adder_out(width-1 downto 0),
             c_out => adder_out(width)
             );
                   
    p_adder_input : process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            adder_in_a_d1 <= adder_in_a;
            adder_in_b_d1 <= adder_in_b;
            --adder_comb <= std_logic_vector(unsigned(adder_comb) + 1);
            --Create adder input stimuli, tried to make it somewhat random
            adder_comb <= lfsr_data & lfsr_data(19 downto 0) & lfsr_data(29 downto 18) & std_logic_vector(shift_right(signed(lfsr_data),2))
                & std_logic_vector(shift_left(unsigned(lfsr_data),1)) & lfsr_data(29 downto 10) & lfsr_data(19 downto 8) & lfsr_data;
            
            --adder_comb(0) <= '1';
            --adder_comb(width) <= '1';
            --adder_comb(width-1 downto 1) <= zeros;
            --adder_comb(adder_comb'LEFT downto width+1) <= zeros;
            
            --Concatenate 0 to get carry out
            adder_validate <= std_logic_vector(unsigned('0' & adder_in_a) + unsigned(adder_in_b));
            adder_validate_d1 <= adder_validate;
            
            --Validate output, concat 0 with adder_validate to get positive integer
            assert unsigned(adder_validate_d1) = unsigned(adder_out) report "Arithmetic error! Expected: "
                & integer'image(to_integer(unsigned('0' & adder_out)))
                & " got: "
                & integer'image(to_integer(unsigned('0' & adder_validate_d1))) severity ERROR;
        end if;
    end process p_adder_input;
    
    

end Behavioral;
