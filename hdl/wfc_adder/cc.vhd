----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/20/2022 10:15:50 PM
-- Design Name: 
-- Module Name: cc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Carry Compression Cell
--              Calculates propagate, p, and generate, g, signals
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cc is
    generic(set_name : string); --not used
    port(
        a   : in std_logic_vector(1 downto 0);
        b   : in std_logic_vector(1 downto 0);
        c_i : in std_logic;
        c_o : out std_logic
        );
end cc;

architecture Behavioral of cc is

    signal p : std_logic := '0';
    signal g : std_logic := '0';

begin

    --propagate signal
    p <= (a(0) xor b(0)) and (a(1) xor b(1));
    
    --generate signal
    g <= (a(1) and b(1)) or ((a(1) or b(1)) and a(0));
    
    --mux
    c_o <= g when p = '0' else c_i;


end Behavioral;
