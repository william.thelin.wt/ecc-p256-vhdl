----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/20/2022 11:44:40 PM
-- Design Name: 
-- Module Name: so - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Sum Out Cell -- so cell
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 

--  Truth Table to determine INIT value for a LUT6_2
--     | '1' a0 b0 a1 b1 c | s1 | 
--      _____________________________
--     | I5 I4 I3 I2 I1 I0 | O6 |
--     ------------------------------        INIT = 32'hC396963C
--     |  1  0  0  0  0  0 | 0 |\                       |
--     |  1  0  0  0  0  1 | 0 | \ = 4'b1100 = 4'hC     |
--     |  1  0  0  0  1  0 | 1 | /                      |
--     |  1  0  0  0  1  1 | 1 |/                       |
--     |-------------------|---|                        |
--     |  1  0  0  1  0  0 | 1 |\                       |
--     |  1  0  0  1  0  1 | 1 | \ = 4'b0011 = 4'h3     |
--     |  1  0  0  1  1  0 | 0 | /                      |
--     |  1  0  0  1  1  1 | 0 |/                       |
--     |-------------------|---|                        |
--     |  1  0  1  0  0  0 | 0 |\                       |
--     |  1  0  1  0  0  1 | 1 | \ = 4'b0110 = 4'h6     |
--     |  1  0  1  0  1  0 | 1 | /                      |
--     |  1  0  1  0  1  1 | 0 |/                       |
--     |-------------------|---|                        |
--     |  1  0  1  1  0  0 | 1 |\                       |
--     |  1  0  1  1  0  1 | 0 | \ = 4'b1001 = 4'h9     |
--     |  1  0  1  1  1  0 | 0 | /                      |
--     |  1  0  1  1  1  1 | 1 |/                       |
--     -------------------------                        |
--     |  1  1  0  0  0  0 | 0 |\                       |
--     |  1  1  0  0  0  1 | 1 | \ = 4'b0110 = 4'h6     |
--     |  1  1  0  0  1  0 | 1 | /                      |
--     |  1  1  0  0  1  1 | 0 |/                       |
--     |-------------------|---|                        |
--     |  1  1  0  1  0  0 | 1 |\                       |
--     |  1  1  0  1  0  1 | 0 | \ = 4'b1001 = 4'h9     |
--     |  1  1  0  1  1  0 | 0 | /                      |
--     |  1  1  0  1  1  1 | 1 |/                       |
--     |-------------------|---|                        |
--     |  1  1  1  0  0  0 | 1 |\                       |
--     |  1  1  1  0  0  1 | 1 | \ = 4'b0011 = 4'h3     |
--     |  1  1  1  0  1  0 | 0 | /                      |
--     |  1  1  1  0  1  1 | 0 |/                       |
--     |-------------------|---|                        |
--     |  1  1  1  1  0  0 | 0 |\                       |
--     |  1  1  1  1  0  1 | 0 | \ = 4'b1100 = 4'hC ----+
--     |  1  1  1  1  1  0 | 1 | /
--     |  1  1  1  1  1  1 | 1 |/
--     ------------------------

--     | '1' a0 b0 a1 b1 c | s0 |
--      _____________________________
--     | I5 I4 I3 I2 I1 I0 | O5 |
--     ------------------------------        INIT = 32'hAA5555AA
--     |  1  0  0  0  0  0 | 0 |\                       |
--     |  1  0  0  0  0  1 | 1 | \ = 4'b1010 = 4'hA     |
--     |  1  0  0  0  1  0 | 0 | /                      |
--     |  1  0  0  0  1  1 | 1 |/                       |
--     |-------------------|---|                        |
--     |  1  0  0  1  0  0 | 0 |\                       |
--     |  1  0  0  1  0  1 | 1 | \ = 4'b1010 = 4'hA     |
--     |  1  0  0  1  1  0 | 0 | /                      |
--     |  1  0  0  1  1  1 | 1 |/                       |
--     |-------------------|---|                        |
--     |  1  0  1  0  0  0 | 1 |\                       |
--     |  1  0  1  0  0  1 | 0 | \ = 4'b0101 = 4'h5     |
--     |  1  0  1  0  1  0 | 1 | /                      |
--     |  1  0  1  0  1  1 | 0 |/                       |
--     |-------------------|---|                        |
--     |  1  0  1  1  0  0 | 1 |\                       |
--     |  1  0  1  1  0  1 | 0 | \ = 4'b0101 = 4'h5     |
--     |  1  0  1  1  1  0 | 1 | /                      |
--     |  1  0  1  1  1  1 | 0 |/                       |
--     -------------------------                        |
--     |  1  1  0  0  0  0 | 1 |\                       |
--     |  1  1  0  0  0  1 | 0 | \ = 4'b0101 = 4'h5     |
--     |  1  1  0  0  1  0 | 1 | /                      |
--     |  1  1  0  0  1  1 | 0 |/                       |
--     |-------------------|---|                        |
--     |  1  1  0  1  0  0 | 1 |\                       |
--     |  1  1  0  1  0  1 | 0 | \ = 4'b0101 = 4'h5     |
--     |  1  1  0  1  1  0 | 1 | /                      |
--     |  1  1  0  1  1  1 | 0 |/                       |
--     |-------------------|---|                        |
--     |  1  1  1  0  0  0 | 0 |\                       |
--     |  1  1  1  0  0  1 | 1 | \ = 4'b1010 = 4'hA     |
--     |  1  1  1  0  1  0 | 0 | /                      |
--     |  1  1  1  0  1  1 | 1 |/                       |
--     |-------------------|---|                        |
--     |  1  1  1  1  0  0 | 0 |\                       |
--     |  1  1  1  1  0  1 | 1 | \ = 4'b1010 = 4'hA ----+
--     |  1  1  1  1  1  0 | 0 | /
--     |  1  1  1  1  1  1 | 1 |/
--     ------------------------

----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity so_lut is
    generic(
        set_name    : string;
        rloc_index  : integer
        );
    port(
        clk : in std_logic;
        clear : in std_logic;
        a   : in std_logic_vector (1 downto 0);
        b   : in std_logic_vector (1 downto 0);
        c   : in std_logic;
        s   : out std_logic_vector (1 downto 0) := (others => '0')
        );
end so_lut;

architecture Behavioral of so_lut is
    
    signal a0, a1, b0, b1, s0, s1 : std_logic := '0';
    
    --RLOC attributes
    attribute U_SET : string;
    attribute RLOC  : string;
    
    attribute U_SET of LUT6_2_inst  : label is set_name;
    attribute RLOC of LUT6_2_inst   : label is "X1Y" & integer'IMAGE(rloc_index);
    attribute U_SET of s  : signal is set_name;
    attribute RLOC of s   : signal is "X1Y" & integer'IMAGE(rloc_index);

begin

    a0 <= a(0);
    a1 <= a(1);
    b0 <= b(0);
    b1 <= b(1);
    --s(0) <= s0;
    --s(1) <= s1;

   -- LUT6_2: 6-input  2 output Look-Up Table
   --         Virtex UltraScale+
   -- Xilinx HDL Language Template, version 2021.2

   LUT6_2_inst : LUT6_2
   generic map (
      INIT => X"C396963CAA5555AA") -- Specify LUT Contents
   port map (
      O6 => s1,  -- 6/5-LUT output (1-bit)
      O5 => s0,  -- 5-LUT output (1-bit)
      I0 => c,   -- LUT input (1-bit)
      I1 => b1,   -- LUT input (1-bit)
      I2 => a1,   -- LUT input (1-bit)
      I3 => b0,   -- LUT input (1-bit)
      I4 => a0,   -- LUT input (1-bit)
      I5 => '1'    -- LUT input (1-bit)
   );
   
    p_ff : process(clk)
    begin
        if rising_edge(clk) then
            if(clear = '1') then
                s <= "00";
            else
                s(0) <= s0;
                s(1) <= s1;
            end if;
        end if;
    end process p_ff;
    


end Behavioral;
