----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/20/2022 11:44:40 PM
-- Design Name: 
-- Module Name: so - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Sum Out Cell -- so cell
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
-- use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fa_reg is
    generic(
        set_name    : string;
        rloc_index  : integer
        );
    port(
        clk : in std_logic;
        clear : in std_logic;
        p   : in std_logic;
        q   : out std_logic := '0'
        );
end fa_reg;

architecture Behavioral of fa_reg is
    
    --RLOC attributes
    attribute U_SET : string;
    attribute RLOC  : string;
    
    attribute U_SET of q  : signal is set_name;
    attribute RLOC of q   : signal is "X0Y" & integer'IMAGE(rloc_index);

begin

    p_ff : process(clk)
    begin
        if rising_edge(clk) then
            if(clear = '1') then
                q <= '0';
            else
                q <= p;
            end if;
        end if;
    end process p_ff;


end Behavioral;
