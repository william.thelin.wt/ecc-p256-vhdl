----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/20/2022 10:15:50 PM
-- Design Name: 
-- Module Name: wfc_adder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Wide Fast-Carry adder - WFC-adder
--              Generates the carry signal for wide operands faster architecture
--              ordinary.
--              K_div2 is the number of cc modules for K_div2 > 1
--              width = 8*L, L is an integer

-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

library ecc;
use ecc.all;

entity wfc_adder is
    generic(
        adder_name  : string;
        width       : integer range 48 to 2048 := 128;
        K_div2      : integer range 16 to 1024 := 16
    );
    port(
        clk     : in std_logic;
        clear   : in std_logic;
        a       : in std_logic_vector(width-1 downto 0);
        b       : in std_logic_vector(width-1 downto 0);
        c_in    : in std_logic;
        --output  : out std_logic_vector(width downto 0) := (others => '0')
        output  : out std_logic_vector(width-1 downto 0) := (others => '0');
        c_out   : out std_logic := '0'
        );
end wfc_adder;

architecture Behavioral of wfc_adder is

    constant number_of_LUT6_2_per_slice : integer := 8;
    constant number_of_carry8           : integer := K_div2/8;
    constant number_of_fa               : integer := width - 2*K_div2;
    constant number_of_carry8_fa        : integer := number_of_fa/8;
    
    signal p_fa_vec    : std_logic_vector(number_of_fa-1 downto 0);
    signal g_fa_vec    : std_logic_vector(number_of_fa-1 downto 0);
    signal c_fa_int    : std_logic_vector(number_of_fa-1 downto 0);
    signal s_fa        : std_logic_vector(number_of_fa-1 downto 0);
    signal s_fa_d1     : std_logic_vector(number_of_fa-1 downto 0);
    
    signal c_internal : std_logic_vector(K_div2-1 downto 0);
    signal c_connect : std_logic;
    signal p_vec    : std_logic_vector(K_div2-1 downto 0);
    signal g_vec    : std_logic_vector(K_div2-1 downto 0);
    signal output_int  : std_logic_vector(width downto 0) := (others => '0');
    
    
begin
    
--    g_inferred_adder : if width > 2*K_div2 generate
--        output_int(width downto K_div2*2) <= std_logic_vector(unsigned('0' & a(width-1 downto K_div2*2)) + unsigned('0' & b(width-1 downto K_div2*2)) + ("" & c_internal(K_div2-1)));
--    end generate g_inferred_adder;
--    g_no_inferred_adder : if width = 2*K_div2 generate
--        output_int(width) <= c_internal(K_div2-1);
--    end generate g_no_inferred_adder;
    
--    --DFF output
--    p_output : process(clk)
--        begin
--            if rising_edge(clk) then
--                output(width downto K_div2*2) <= output_int(width downto K_div2*2);
--            end if;
--    end process p_output;

    --output(output'LEFT downto output'LEFT - number_of_fa) <= c_fa_int(c_fa_int'LEFT) & s_fa_d1;
    output(output'LEFT downto output'LEFT - number_of_fa+1) <= s_fa_d1;

----------------------------------------------------------------------------------
-- FA part
----------------------------------------------------------------------------------
    
    --carry output in register
    p_c_out : process(clk)
    begin
        if rising_edge(clk) then
            c_out <= c_fa_int(c_fa_int'LEFT);
        end if;
    end process p_c_out;
    
    g_fa : for k in 0 to number_of_fa-1 generate
        fa : entity ecc.fa_lut
        generic map(
            set_name => adder_name,
            rloc_index => (K_div2 + k)/number_of_LUT6_2_per_slice
            )
        port map(
            a   => a(K_div2*2 + k),
            b   => b(K_div2*2 + k),
            p   => p_fa_vec(k),
            g   => g_fa_vec(k)
            );
    end generate g_fa;
    
    g_carry_fa : for k in 0 to number_of_carry8_fa-1 generate
        g_carry_fa_g : if k > 0 generate
            CARRY8 : entity ecc.carry8_wrapper
            generic map (
                set_name => adder_name,
                rloc_index => number_of_carry8 + k
                )
            port map (    
                c_out => c_fa_int(8*k+7 downto 8*k),  -- 8-bit output: Carry-out
                c_xor_out => s_fa(8*k+7 downto 8*k),  -- 8-bit output: Carry chain XOR data out
                c_in_l => c_fa_int(8*k-1),            -- 1-bit input: Lower Carry-In
                c_in_m => '0',                          -- 1-bit input: Upper Carry-In
                data_in => g_fa_vec(8*k+7 downto 8*k),     -- 8-bit input: Carry-MUX data in
                sel => p_fa_vec(8*k+7 downto 8*k)          -- 8-bit input: Carry-mux select
                );
         end generate g_carry_fa_g;
         
         g_carry_fa_0 : if k = 0 generate
            CARRY8 : entity ecc.carry8_wrapper
            generic map (
                set_name => adder_name,
                rloc_index => number_of_carry8 + k
                )
            port map (    
                c_out => c_fa_int(8*k+7 downto 8*k),  -- 8-bit output: Carry-out
                c_xor_out => s_fa(8*k+7 downto 8*k),  -- 8-bit output: Carry chain XOR data out
                c_in_l => c_internal(c_internal'LEFT),            -- 1-bit input: Lower Carry-In
                c_in_m => '0',                          -- 1-bit input: Upper Carry-In
                data_in => g_fa_vec(8*k+7 downto 8*k),     -- 8-bit input: Carry-MUX data in
                sel => p_fa_vec(8*k+7 downto 8*k)          -- 8-bit input: Carry-mux select
                );
         end generate g_carry_fa_0;
    end generate g_carry_fa;
    
    g_fa_reg : for k in 0 to number_of_fa-1 generate
        fa : entity ecc.fa_reg
        generic map(
            set_name => adder_name,
            rloc_index => (K_div2 + k)/number_of_LUT6_2_per_slice
            )
        port map(
            clk   => clk,
            clear => clear,
            p   => s_fa(k),
            q   => s_fa_d1(k)
            );
    end generate g_fa_reg;
     
----------------------------------------------------------------------------------
-- K_div2 part
----------------------------------------------------------------------------------
    g_carry_cc : for k in 1 to number_of_carry8-1 generate
        CARRY8 : entity ecc.carry8_wrapper
        generic map (
            set_name => adder_name,
            rloc_index => k
            )
        port map (    
            c_out => c_internal(8*k+7 downto 8*k),  -- 8-bit output: Carry-out
            c_xor_out => open,                      -- 8-bit output: Carry chain XOR data out
            c_in_l => c_internal(8*k-1),            -- 1-bit input: Lower Carry-In
            c_in_m => '0',                          -- 1-bit input: Upper Carry-In
            data_in => g_vec(8*k+7 downto 8*k),     -- 8-bit input: Carry-MUX data in
            sel => p_vec(8*k+7 downto 8*k)          -- 8-bit input: Carry-mux select
            );
    end generate g_carry_cc;
    
 
    g_K_div2 : for k in 1 to K_div2-1 generate
        cc : entity ecc.cc_lut
        generic map(
            set_name => adder_name,
            rloc_index => k/number_of_LUT6_2_per_slice
            )
        port map(
            a   => a(k*2+1 downto k*2),
            b   => b(k*2+1 downto k*2),
            p   => p_vec(k),
            g   => g_vec(k)
            );
    
        so : entity ecc.so_lut
        generic map(
            set_name => adder_name,
            rloc_index => k/number_of_LUT6_2_per_slice
            )
        port map(
            clk => clk,
            clear => clear,
            a => a(k*2+1 downto k*2),
            b => b(k*2+1 downto k*2),
            c => c_internal(k-1),
            s => output(k*2+1 downto k*2)
            );
    end generate g_K_div2;    

    cc_0 : entity ecc.cc_lut
    generic map(
        set_name => adder_name,
        rloc_index => 0
        )
    port map(
        a   => a(1 downto 0),
        b   => b(1 downto 0),
        p => p_vec(0),
        g => g_vec(0)
        );
        
    CARRY8_cc_0 : entity ecc.carry8_wrapper
    generic map (
        set_name => adder_name,
        rloc_index => 0
        )
    port map (    
        c_out => c_internal(7 downto 0),        -- 8-bit output: Carry-out
        c_xor_out => open,                      -- 8-bit output: Carry chain XOR data out
        c_in_l => c_in,                          -- 1-bit input: Lower Carry-In
        c_in_m => '0',                          -- 1-bit input: Upper Carry-In
        data_in => g_vec(7 downto 0),           -- 8-bit input: Carry-MUX data in
        sel => p_vec(7 downto 0)                -- 8-bit input: Carry-mux select
        );


    so_0 : entity ecc.so_lut
    generic map(
            set_name => adder_name,
            rloc_index => 0
            )
    port map(
        clk => clk,
        clear => clear,
        a => a(1 downto 0),
        b => b(1 downto 0),
        c => c_in,
        s => output(1 downto 0)
        );


end Behavioral;
