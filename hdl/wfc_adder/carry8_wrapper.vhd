----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/27/2022 10:23:58 PM
-- Design Name: 
-- Module Name: carry8_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity carry8_wrapper is
    generic(
        set_name    : string;
        rloc_index  : integer
        );
    port(
        c_out       : out std_logic_vector(7 downto 0); -- 8-bit output: Carry-out
        c_xor_out   : out std_logic_vector(7 downto 0); -- 8-bit output: Carry chain XOR data out
        c_in_l      : in std_logic;                    -- 1-bit input: Lower Carry-In
        c_in_m      : in std_logic;                    -- 1-bit input: Upper Carry-In
        data_in     : in std_logic_vector(7 downto 0); -- 8-bit input: Carry-MUX data in
        sel         : in std_logic_vector(7 downto 0)  -- 8-bit input: Carry-mux select
    );
end carry8_wrapper;

architecture Behavioral of carry8_wrapper is

    --RLOC attributes
    attribute U_SET : string;
    attribute RLOC  : string;
    
    attribute U_SET of CARRY8_inst  : label is set_name;
    attribute RLOC of CARRY8_inst   : label is "X0Y" & integer'IMAGE(rloc_index);

begin

    -- CARRY8: Fast Carry Logic with Look Ahead
    --         Virtex UltraScale+
    -- Xilinx HDL Language Template, version 2021.2

    CARRY8_inst : CARRY8
       generic map (
            CARRY_TYPE => "SINGLE_CY8"  -- 8-bit or dual 4-bit carry (DUAL_CY4, SINGLE_CY8)
            )
        port map (  
            CO => c_out,         -- 8-bit output: Carry-out
            O => c_xor_out,           -- 8-bit output: Carry chain XOR data out
            CI => c_in_l,         -- 1-bit input: Lower Carry-In
            CI_TOP => c_in_m,          -- 1-bit input: Upper Carry-In
            DI => data_in,         -- 8-bit input: Carry-MUX data in
            S => sel            -- 8-bit input: Carry-mux select
          );

end Behavioral;
