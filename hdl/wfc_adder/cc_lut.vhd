----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/20/2022 10:15:50 PM
-- Design Name: 
-- Module Name: cc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Carry Compression Cell
--              Calculates propagate, p, and generate, g, signals
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity cc_lut is
    generic(
        set_name    : string;
        rloc_index  : integer
        );
    port(
        a   : in std_logic_vector(1 downto 0);
        b   : in std_logic_vector(1 downto 0);
        p   : out std_logic;
        g   : out std_logic
        );
end cc_lut;

architecture Behavioral of cc_lut is
    

    -- The following constants are defined to allow for
    --   equation-based INIT specification for a LUT6.
--    constant I0 : BIT_VECTOR(63 downto 0) := X"AAAAAAAAAAAAAAAA";
--    constant I1 : BIT_VECTOR(63 downto 0) := X"CCCCCCCCCCCCCCCC";
--    constant I2 : BIT_VECTOR(63 downto 0) := X"F0F0F0F0F0F0F0F0";
--    constant I3 : BIT_VECTOR(63 downto 0) := X"FF00FF00FF00FF00";
--    constant I4 : BIT_VECTOR(63 downto 0) := X"FFFF0000FFFF0000";
--    constant I5 : BIT_VECTOR(63 downto 0) := X"FFFFFFFF00000000";

    constant I0 : BIT_VECTOR(31 downto 0) := X"AAAAAAAA";
    constant I1 : BIT_VECTOR(31 downto 0) := X"CCCCCCCC";
    constant I2 : BIT_VECTOR(31 downto 0) := X"F0F0F0F0";
    constant I3 : BIT_VECTOR(31 downto 0) := X"FF00FF00";
    constant I4 : BIT_VECTOR(31 downto 0) := X"FFFF0000";
    
    constant INIT_O6 : BIT_VECTOR(31 downto 0) := (I0 xor I2) and (I1 xor I3);
    constant INIT_O5 : BIT_VECTOR(31 downto 0) := (I1 and I3) or ((I1 or I3) and I0);
    constant INIT_LUT6_2 : BIT_VECTOR(63 downto 0) := INIT_O6 & INIT_O5;
   
    signal O6 : std_logic;
    signal O5 : std_logic;
    
    
    --RLOC attributes
    attribute U_SET : string;
    attribute RLOC  : string;
    
    attribute U_SET of LUT6_2_inst  : label is set_name;
    attribute RLOC of LUT6_2_inst   : label is "X0Y" & integer'IMAGE(rloc_index);
begin

    --Original boolean equations
    --propagate signal
    --p <= (a(0) xor b(0)) and (a(1) xor b(1));
    
    --generate signal
    --g <= (a(1) and b(1)) or ((a(1) or b(1)) and a(0));
    
    
    p <= O6;
    g <= O5;
   -- LUT6_2: 6-input  2 output Look-Up Table
   --         Virtex UltraScale+
   -- Xilinx HDL Language Template, version 2021.2
   LUT6_2_inst : LUT6_2
   generic map (
      INIT => INIT_LUT6_2) -- Specify LUT Contents
   port map (
      O6 => O6,  -- 6/5-LUT output (1-bit)
      O5 => O5,  -- 5-LUT output (1-bit)
      I0 => a(0),   -- LUT input (1-bit)
      I1 => a(1),   -- LUT input (1-bit)
      I2 => b(0),   -- LUT input (1-bit)
      I3 => b(1),   -- LUT input (1-bit)
      I4 => '0',   -- LUT input (1-bit)
      I5 => '1'    -- LUT input (1-bit), set to '1' because we always want O6 to be the output from LUT5_1
   );
   
   
   --mux
   -- c_o <= g when p = '0' else c_i;


end Behavioral;
