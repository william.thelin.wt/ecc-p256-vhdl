----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/17/2022 10:48:47 PM
-- Design Name: 
-- Module Name: wfc_p_adder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library ecc;
use ecc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity wfc_p_adder is
    generic(
        adder_name  : string;
        width       : integer range 48 to 2048 := 128;
        K_div2      : integer range 16 to 1024 := 16
    );
    port(
        clk     : in std_logic;
        a       : in std_logic_vector(width-1 downto 0);
        b       : in std_logic_vector(width-1 downto 0);
        c_in    : in std_logic;
        --output  : out std_logic_vector(width downto 0) := (others => '0')
        output  : out std_logic_vector(width-1 downto 0) := (others => '0');
        c_out   : out std_logic := '0'
        );
end wfc_p_adder;

architecture Behavioral of wfc_p_adder is

    constant half_width : integer := width/2;
    
    signal a_upper : std_logic_vector(width/2-1 downto 0) := (others => '0');
    signal a_upper_d1 : std_logic_vector(width/2-1 downto 0) := (others => '0');
    signal a_lower : std_logic_vector(width/2-1 downto 0) := (others => '0');
    signal b_upper : std_logic_vector(width/2-1 downto 0) := (others => '0');
    signal b_upper_d1 : std_logic_vector(width/2-1 downto 0) := (others => '0');
    signal b_lower : std_logic_vector(width/2-1 downto 0) := (others => '0');
    signal output_upper : std_logic_vector(width/2-1 downto 0) := (others => '0');
    signal output_lower : std_logic_vector(width/2-1 downto 0) := (others => '0');
    signal output_lower_d1 : std_logic_vector(width/2-1 downto 0) := (others => '0');
    signal carry_internal : std_logic := '0';

begin

    a_upper <= a(width-1 downto half_width);
    b_upper <= b(width-1 downto half_width);
    a_lower <= a(half_width-1 downto 0);
    b_lower <= b(half_width-1 downto 0);
    output <= output_upper & output_lower_d1;
    
    
    wfc_adder_upper : entity ecc.wfc_adder
        generic map(
                adder_name => adder_name & "_u",
                width => half_width,
                K_div2 => K_div2)
        port map(
             clk => clk,
             clear => '0',
             a => a_upper_d1,
             b => b_upper_d1,
             c_in => carry_internal,
             output => output_upper,
             c_out => c_out
             );
             
     wfc_adder_lower : entity ecc.wfc_adder
        generic map(
                adder_name => adder_name & "_l",
                width => half_width,
                K_div2 => K_div2)
        port map(
             clk => clk,
             clear => '0',
             a => a_lower,
             b => b_lower,
             c_in => c_in,
             output => output_lower,
             c_out => carry_internal
             );
             
      p_pipe : process(clk)
      begin
        if rising_edge(clk) then
            a_upper_d1 <= a_upper;
            b_upper_d1 <= b_upper;
            output_lower_d1 <= output_lower;
        end if;
      end process p_pipe;


end Behavioral;
