----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/20/2022 11:44:40 PM
-- Design Name: 
-- Module Name: so - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Sum Out Cell -- so cell
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity so is
    Port ( a : in STD_LOGIC_VECTOR (1 downto 0);
           b : in STD_LOGIC_VECTOR (1 downto 0);
           c : in STD_LOGIC;
           s : out STD_LOGIC_VECTOR (1 downto 0));
end so;

architecture Behavioral of so is
    
    signal tmp_s : std_logic_vector(2 downto 0) := (others => '0');

begin

    s <= std_logic_vector(unsigned(a) + unsigned(b) + ("" & c));
    


end Behavioral;
