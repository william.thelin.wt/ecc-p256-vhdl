----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/20/2022 10:15:50 PM
-- Design Name: 
-- Module Name: wfc_adder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Wide Fast-Carry adder - WFC-adder
--              Generates the carry signal for wide operands faster architecture
--              ordinary.
--              K_div2 is the number of cc modules for K_div2 > 1
--              width = 8*L, L is an integer

-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

library ecc;
use ecc.all;

entity wfc_acc is
    generic(
        acc_name  : string;
        width       : integer range 48 to 2048 := 128;
        K_div2      : integer range 16 to 1024 := 16
    );
    port(
        clk     : in std_logic;
        clear   : in std_logic;
        din     : in std_logic_vector(width-1 downto 0);
        output  : out std_logic_vector(width-1 downto 0) := (others => '0')
        );
end wfc_acc;

architecture Behavioral of wfc_acc is

    
    signal acc : std_logic_vector(width-1 downto 0) := (others => '0');
    
begin
  
    output <= acc;
  
    wfc_adder_inst : entity ecc.wfc_adder
        generic map(
                adder_name => acc_name,
                width => width,
                K_div2 => K_div2)
        port map(
             clk => clk,
             clear => clear,
             a => din,
             b => acc,
             c_in => '0',
             output => acc,
             c_out => open
             );

end Behavioral;
