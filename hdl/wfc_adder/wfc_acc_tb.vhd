----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/21/2022 09:16:14 PM
-- Design Name: 
-- Module Name: wfc_adder_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library ecc;
use ecc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity wfc_acc_tb is
--  Port ( );
end wfc_acc_tb;

architecture Behavioral of wfc_acc_tb is

    constant period : time := 10 ns;
    constant width : integer := 48*2;
    constant K_div2 : integer := 8*2;

    signal sys_clk : std_logic := '0';
    
    --signal adder_comb   : std_logic_vector(2*width-1 downto 0) := X"FFFF_FFFF_FFFF" & X"FFFF_FFFF_FFF0" & X"FFFF_FFFF_FFFF" & X"FFFF_FFFF_FFF0";
    signal adder_comb   : std_logic_vector(2*width-1 downto 0) := (others => '0');
    signal lfsr_data    : std_logic_vector(31 downto 0) := (others => '0');
    signal clear        : std_logic := '0';
    signal adder_in_a   : std_logic_vector(width-1 downto 0) := (others => '0');
    signal adder_in_b   : std_logic_vector(width-1 downto 0) := (others => '0');
    signal adder_in_a_d1   : std_logic_vector(width-1 downto 0) := (others => '0');
    signal adder_in_b_d1   : std_logic_vector(width-1 downto 0) := (others => '0');
    signal adder_out    : std_logic_vector(width downto 0) := (others => '0');
    signal adder_out_glob    : std_logic_vector(width downto 0) := (others => '0');
    
    signal adder_validate : std_logic_vector(width downto 0) := (others => '0');
    signal adder_validate_d1 : std_logic_vector(width downto 0) := (others => '0');
    
    signal shift_left_out : std_logic_vector(width-1 downto 0) := (others => '0');
    signal shift_steps : std_logic_vector(3 downto 0) := (others => '0');
    signal din_shifter   : std_logic_vector(width-1 downto 0) := (47 downto 36 => '1', 0 => '1' ,others => '0');

    constant ones : std_logic_vector(width-2 downto 0) := (others => '1');
    constant zeros : std_logic_vector(width-2 downto 0) := (others => '0');

begin

    sys_clk <= not sys_clk after period/2;
    
    adder_in_a <= adder_comb(width-1 downto 0);
    adder_in_b <= adder_comb(2*width-1 downto width);
    
    
    LFSR_inst : entity ecc.LFSR
        generic map(
            width => 32,
            init => X"F0F1F0F1")
        port map(
            clk => sys_clk,
            lfsr_data => lfsr_data
            );
    
    wfc_acc_inst : entity ecc.wfc_acc
        generic map(
                acc_name => "my_wfc",
                width => width,
                K_div2 => K_div2)
        port map(
             clk => sys_clk,
             clear => clear,
             din => adder_in_a,
             output => adder_out(width-1 downto 0)
             );
             
             
    --testing k_shifter
    k_shifter_inst : entity ecc.k_shifter
        generic map(
          width_in => width/2,
          width_out => width,
          shift_count => 10,
          log2_shift_count => 4,
          k => 6
          )
        port map(
             clk => sys_clk,
             din => din_shifter(width/2-1 downto 0),--adder_in_a(width/2-1 downto 0),
             shift_in => shift_steps,
             output => shift_left_out
             );
    --k_shifter stimuli
    shift_steps <= x"1" after period*4, x"2" after period*5,
                   x"3" after period*6, x"4" after period*7,
                   x"5" after period*8, x"6" after period*9,
                   x"7" after period*10, x"8" after period*11,
                   x"9" after period*12;

                   
    p_adder_input : process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            adder_in_a_d1 <= adder_in_a;
            adder_in_b_d1 <= adder_in_b;
            --adder_comb <= std_logic_vector(unsigned(adder_comb) + 1);
            --Create adder input stimuli, tried to make it somewhat random
            
            adder_comb(0) <= '1';
            adder_comb(32 downto 1) <= lfsr_data;
            
            if (adder_comb(11) = '1' and adder_comb(27) = '1') then
                clear <= '1';
            else
                clear <= '0';
            end if;
            
            --Concatenate 0 to get carry out
            if(clear = '1') then
                adder_validate <= std_logic_vector(to_unsigned(0, width+1));
            else
                adder_validate <= std_logic_vector(unsigned(adder_validate) + unsigned('0' & adder_in_a));
            end if;
            
            --Validate output, concat 0 with adder_validate to get positive integer
            assert unsigned(adder_validate) = unsigned(adder_out) report "Arithmetic error! Expected: "
                & integer'image(to_integer(unsigned('0' & adder_out)))
                & " got: "
                & integer'image(to_integer(unsigned('0' & adder_validate_d1))) severity ERROR;
        end if;
    end process p_adder_input;
    
    

end Behavioral;
