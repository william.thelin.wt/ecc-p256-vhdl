----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/02/2022 10:16:30 PM
-- Design Name: 
-- Module Name: fa_lut - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity fa_lut is
    generic(
        set_name    : string;
        rloc_index  : integer
        );
    port(
        a   : in std_logic;
        b   : in std_logic;
        p   : out std_logic;
        g   : out std_logic
        );
end fa_lut;

architecture Behavioral of fa_lut is
    
    constant I0 : BIT_VECTOR(31 downto 0) := X"AAAAAAAA";
    constant I1 : BIT_VECTOR(31 downto 0) := X"CCCCCCCC";
    constant I2 : BIT_VECTOR(31 downto 0) := X"F0F0F0F0";
    constant INIT_LUT6_2 : BIT_VECTOR(63 downto 0) := (I0 xor I1) & I2;
    
    --RLOC attributes
    attribute U_SET : string;
    attribute RLOC  : string;
    
    attribute U_SET of LUT6_2_inst  : label is set_name;
    attribute RLOC of LUT6_2_inst   : label is "X0Y" & integer'IMAGE(rloc_index);

begin

   -- LUT6_2: 6-input  2 output Look-Up Table
   --         Virtex UltraScale+
   -- Xilinx HDL Language Template, version 2021.2
   LUT6_2_inst : LUT6_2
   generic map (
      INIT => INIT_LUT6_2) -- Specify LUT Contents
   port map (
      O6 => p,  -- 6/5-LUT output (1-bit)
      O5 => g,  -- 5-LUT output (1-bit)
      I0 => a,   -- LUT input (1-bit)
      I1 => b,   -- LUT input (1-bit)
      I2 => a,   -- LUT input (1-bit)
      I3 => '0',   -- LUT input (1-bit)
      I4 => '0',   -- LUT input (1-bit)
      I5 => '1'    -- LUT input (1-bit), set to '1' because we always want O6 to be the output from LUT5_1
      );

end Behavioral;
