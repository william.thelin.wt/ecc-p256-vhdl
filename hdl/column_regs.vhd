----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/19/2022 05:23:53 PM
-- Design Name: 
-- Module Name: column_regs - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Creates a column of regs relative to y and x start and some set name
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library ecc;
use ecc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity column_regs is
    generic(
        rloc_set_name   : string;
        rloc_y_start    : integer;
        rloc_x_start    : integer;
        regs_per_slice  : integer range 1 to 16 := 16;
        width           : integer
    );
    port ( clk : in STD_LOGIC;
           p_vec : in STD_LOGIC_VECTOR (width-1 downto 0);
           q_vec : out STD_LOGIC_VECTOR (width-1 downto 0));
end column_regs;

architecture Behavioral of column_regs is

begin

    g_adder_input_regs : for k in 0 to width-1 generate
        rloc_reg_inst : entity ecc.rloc_reg
        generic map(
            set_name => rloc_set_name,
            rloc_index_y => k/regs_per_slice,
            rloc_index_x => rloc_x_start
            )
        port map(
            clk   => clk,
            p   => p_vec(k),
            q   => q_vec(k)
            );
    end generate g_adder_input_regs; 


end Behavioral;
