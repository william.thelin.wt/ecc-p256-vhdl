----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/19/2022 04:20:32 PM
-- Design Name: 
-- Module Name: ecc_p256_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library ecc;
use ecc.all;

library xil_defaultlib;
use xil_defaultlib.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ecc_p256_top is
    port(
        --system clocks from Si5394J
        SYSCLK2_N : in std_logic;
        SYSCLK2_P : in std_logic;
        SYSCLK3_N : in std_logic;
        SYSCLK3_P : in std_logic;

        --PCIE clocks from Si5394J
        PCIE_REFCLK0_N : in std_logic;
        PCIE_REFCLK0_P : in std_logic;
        PCIE_SYSCLK0_N : in std_logic;
        PCIE_SYSCLK0_P : in std_logic;
        
        PCIE_REFCLK1_N : in std_logic;
        PCIE_REFCLK1_P : in std_logic;
        PCIE_SYSCLK1_N : in std_logic;
        PCIE_SYSCLK1_P : in std_logic;

        --QSFP clocks from 
        SYNCE_CLK0_N : in std_logic;
        SYNCE_CLK0_P : in std_logic;
        SYNCE_CLK1_N : in std_logic;
        SYNCE_CLK1_P : in std_logic
    );
end ecc_p256_top;

architecture Behavioral of ecc_p256_top is

    constant width : integer := 256;
    constant K_div2 : integer := 8*6;
    constant number_of_LUT6_2_per_slice : integer := 8;
    constant number_of_regs_per_slice : integer := 16;
    constant adder_name : string := "wfc_0";


    --System clock
    signal sys_clk          : std_logic;
    signal sys_clk_locked   : std_logic := '0';
    
    signal sys_reset : std_logic := '0';
    
    --Adder signals
    signal adder_in_a   : std_logic_vector(width-1 downto 0) := (others => '0');
    signal mem_a_addr   : unsigned(0 downto 0) := (others => '0');
    signal adder_in_b   : std_logic_vector(width-1 downto 0) := (0 => '1', others => '0');
    signal mem_b_addr   : unsigned(0 downto 0) := (others => '0');
    signal mem_comb_addr    : unsigned(1 downto 0) := (others => '0');
    signal adder_out    : std_logic_vector(width-1 downto 0);
    signal adder_out_glob : std_logic_vector(width-1 downto 0) := (others => '0');
    signal adder_c_out  : std_logic := '0';
    signal adder_ce     : std_logic_vector(11 downto 0) := (others => '0');
    signal meme_a_dout  : std_logic_vector(width-1 downto 0) := (others => '0');
    signal meme_b_dout  : std_logic_vector(width-1 downto 0) := (others => '0');
    
    
    attribute keep: boolean;
    attribute keep of adder_out: signal is true;
    attribute keep of adder_c_out: signal is true;
    
    attribute dont_touch: boolean;
    attribute dont_touch of adder_out: signal is true;
    attribute dont_touch of adder_c_out: signal is true;
    attribute dont_touch of adder_out_glob: signal is true;
    
    
    
    
    --RLOC attributes
    attribute U_SET : string;
    attribute RLOC  : string;
    
    constant set_name_a    : string := "wfc_0_l_wfc_adder_set";
    constant set_name_b    : string := "wfc_0_l_wfc_adder_set";
    
--    attribute U_SET of adder_in_a : signal is set_name_a;
--    attribute RLOC of adder_in_a   : signal is "X-1Y";
--    attribute U_SET of adder_in_b : signal is set_name_b;
--    attribute RLOC of adder_in_b   : signal is "X-1Y";


begin

    sys_reset <= sys_clk_locked;

    --Generates internal clocks from 100MHz ref clock
    system_clock_gen_inst : entity xil_defaultlib.system_clock_gen
        port map(
            clk_in1_n => SYSCLK2_N,
            clk_in1_p => SYSCLK2_P,
            clk_out1 => sys_clk,
            locked => sys_clk_locked
        );

--    adder_128_inst : entity xil_defaultlib.adder_128
--        PORT MAP (
--            A => adder_in_a,
--            B => adder_in_b,
--            CLK => sys_clk,
--            CE => adder_ce(0),
--            C_OUT => adder_c_out,
--            S => adder_out
--        );
    
--    adder_128_inst : entity ecc.g_adder
--        generic map(width => width)
--        port map(
--            clk => sys_clk,
--            a => adder_in_a,
--            b => adder_in_b,
--            output => adder_out
--        );

    mem_a_addr <= mem_comb_addr(0 downto 0);
    mem_b_addr <= mem_comb_addr(1 downto 1);

    a_mem : entity xil_defaultlib.adder_mem
        port map(
            clka => sys_clk,
            ena => '1',
            wea(0) => '0',
            addra => std_logic_vector(mem_a_addr),
            dina => (others => '0'),
            douta(width-1 downto 0) => meme_a_dout,
            douta(511 downto width) => open
        );
        
    b_mem : entity xil_defaultlib.adder_mem
        port map(
            clka => sys_clk,
            ena => '1',
            wea(0) => '0',
            addra => std_logic_vector(mem_b_addr),
            dina => (others => '0'),
            douta(width-1 downto 0) => meme_b_dout,
            douta(511 downto width) => open
        );


    column_regs_a : entity ecc.column_regs
        generic map(
                rloc_set_name => adder_name,
                rloc_y_start => 0,
                rloc_x_start => -2,
                regs_per_slice => number_of_regs_per_slice,
                width => width)
        port map(
             clk => sys_clk,
             p_vec => meme_a_dout,
             q_vec => adder_in_a
             );
             
    column_regs_b : entity ecc.column_regs
        generic map(
                rloc_set_name => adder_name,
                rloc_y_start => 0,
                rloc_x_start => -1,
                regs_per_slice => number_of_regs_per_slice,
                width => width)
        port map(
             clk => sys_clk,
             p_vec => meme_b_dout,
             q_vec => adder_in_b
             );


    wfc_adder_inst : entity ecc.wfc_adder
        generic map(
                adder_name => adder_name,
                width => width,
                K_div2 => K_div2)
        port map(
             clk => sys_clk,
             a => adder_in_a,
             b => adder_in_b,
             c_in => '0',
             output => adder_out,
             c_out => open
             );

--    wfc_p_adder_inst : entity ecc.wfc_p_adder
--        generic map(
--                adder_name => "wfc_0",
--                width => width,
--                K_div2 => K_div2)
--        port map(
--             clk => sys_clk,
--             a => adder_in_a,
--             b => adder_in_b,
--             c_in => '0',
--             output => adder_out,
--             c_out => open
--             );

--    rloc_adder_inst : entity ecc.rloc_adder
--        generic map(
--                adder_name => "rloc_0",
--                width => width
--                )
--        port map(
--             clk => sys_clk,
--             a => adder_in_a,
--             b => adder_in_b,
--             output => adder_out
--             );
                   
    p_adder_input : process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            --adder_in_a <= meme_a_dout;
            --adder_in_b <= meme_b_dout;
            mem_comb_addr <= mem_comb_addr + 1;
            adder_ce(0) <= '1'; --always enable
            adder_ce(11 downto 1) <= adder_ce(10 downto 0);
            adder_out_glob <= adder_out;
        end if;
    end process p_adder_input;
    

end Behavioral;
