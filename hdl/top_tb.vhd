----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/20/2022 12:02:36 AM
-- Design Name: 
-- Module Name: top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library ecc;
use ecc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;



entity top_tb is
--  Port ( );
end top_tb;

architecture Behavioral of top_tb is

    constant period_100MHz : time := 10 ns;

    
    signal sys_clk_100MHz : std_logic := '0';
    signal sys_clk_100MHz_p : std_logic := '1';
    signal sys_clk_100MHz_n : std_logic := '0';

begin

    sys_clk_100MHz <= not sys_clk_100MHz after period_100MHz/2;



end Behavioral;
