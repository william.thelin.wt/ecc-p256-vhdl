----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/12/2022 11:00:05 AM
-- Design Name: 
-- Module Name: LFSR - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
 
entity LFSR is
  generic (
    width : integer := 32;
    init  : std_logic_vector(31 downto 0) := (others => '1')
    );
  port (
    clk    : in std_logic;
    lfsr_data : out std_logic_vector(width-1 downto 0)
    );
end entity LFSR;
 
architecture Behavioral of LFSR is
 
  signal r_LFSR : std_logic_vector(width downto 1) := (others => '0');
  signal w_XNOR : std_logic;
   
begin
 
  -- Purpose: Load up LFSR with Seed if Data Valid (DV) pulse is detected.
  -- Othewise just run LFSR when enabled.
  p_LFSR : process (clk) is
  begin
    if rising_edge(clk) then
          r_LFSR <= r_LFSR(r_LFSR'left-1 downto 1) & w_XNOR;
    end if;
  end process p_LFSR;
  
  w_XNOR <= r_LFSR(32) xnor r_LFSR(22) xnor r_LFSR(2) xnor r_LFSR(1);
  
  lfsr_data <= r_LFSR(r_LFSR'left downto 1);


end Behavioral;
