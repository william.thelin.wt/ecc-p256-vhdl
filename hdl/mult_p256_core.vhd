----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2022-03-28 09:25:33 PM
-- Design Name: 
-- Module Name: mult_p256_core - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

library ecc;
use ecc.all;
use ecc.ecc_pkg.all;

library xil_defaultlib;
use xil_defaultlib.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mult_p256_core is
  generic(
    mult_width : integer := 256;
    width      : integer := 512;
    mult_name  : string;
    K_div2     : integer := 8*26
    );
  port (
    sys_clk  : in  std_logic;
    a_input  : in  std_logic_vector(mult_width-1 downto 0);
    b_input  : in  std_logic_vector(mult_width-1 downto 0);
    din_vld  : in  std_logic;
    rdy_out  : out std_logic                          := '1';
    dout     : out std_logic_vector(width-1 downto 0) := (others => '0');
    dout_vld : out std_logic                          := '0'
    );
end mult_p256_core;

architecture Behavioral of mult_p256_core is
  
  constant adder_name : string  := mult_name & "_wfc_adder";
  constant acc_name   : string  := mult_name & "_wfc_acc";
  constant latency    : integer := 8;--2*(word_count_M-1) + word_count_N-word_count_M+1;
  constant mult_latency : integer := 4;

  --Signals
  signal mult_a_in_a    : mult_N_array_t                              := (others => (others => '0'));
  signal mult_a_in_b    : mult_M_array_t                              := (others => (others => '0'));
  signal mult_b_in_a    : mult_N_array_t                              := (others => (others => '0'));
  signal mult_b_in_b    : mult_M_array_t                              := (others => (others => '0'));
  signal shifter_a_in   : std_logic_vector(mult_out_width-1 downto 0) := (others => '0');
  signal shifter_b_in   : std_logic_vector(mult_out_width-1 downto 0) := (others => '0');
  signal shifter_dout_a : std_logic_vector(width-1 downto 0)          := (others => '0');
  signal shifter_dout_b : std_logic_vector(width-1 downto 0)          := (others => '0');
  signal shift_steps_a  : std_logic_vector(3 downto 0)                := "0000";
  signal shift_steps_b  : std_logic_vector(3 downto 0)                := "0000";
  signal clear          : std_logic                                   := '0';
  signal adder_in_a     : std_logic_vector(width-1 downto 0)          := (others => '0');
  signal adder_in_b     : std_logic_vector(width-1 downto 0)          := (others => '0');
  signal adder_out      : std_logic_vector(width-1 downto 0);
  signal adder_in_acc   : std_logic_vector(width-1 downto 0)          := (others => '0');

  --Control signals
  signal vld_pipe : std_logic_vector(latency-1 downto 0) := (others => '0');
  signal mult_active : std_logic := '0';

begin

  vld_pipe(0) <= din_vld;
  dout_vld <= vld_pipe(latency-1);

  shift_steps_a <= "0000";
  shift_steps_b <= "0000";


  -- Control logic not complete
  p_control : process(sys_clk)
  begin
    if rising_edge(sys_clk) then
      vld_pipe(latency-1 downto 1) <= vld_pipe(latency-2 downto 0);

      if(vld_pipe(mult_latency-1 downto 0) = std_logic_vector(to_unsigned(0,mult_latency))) then
        rdy_out <= '1';
      else
        rdy_out <= '0';
      end if;

      --Clear acc register after valid output
      clear <= vld_pipe(vld_pipe'LEFT);
    end if;
  end process p_control;
  
  

  g_mult_a : for I in 0 to word_count_M-1 generate
    g_input_zero : if I = 0 generate
      mult_a_in_a(I) <= a_input(mult_width-1 downto mult_width-mult_width_N);
      mult_a_in_b(I) <= b_input(mult_width-1 downto mult_width-mult_width_M);
    end generate g_input_zero;
    g_input : if I > 0 generate
      mult_a_in_a(I) <= mult_N_zeros;
      mult_a_in_b(I) <= mult_M_zeros;
    end generate g_input;

    mult_a_inst : entity xil_defaultlib.mult_17x26
      port map(
        clk => sys_clk,
        a   => mult_a_in_a(I),
        b   => mult_a_in_b(I),
        p   => shifter_a_in((I+1)*(mult_width_N+mult_width_M)-1 downto I*(mult_width_N+mult_width_M))
        );
  end generate g_mult_a;

  g_mult_b : for I in 0 to word_count_M-1 generate
    g_input_zero : if I = 0 generate
      mult_b_in_a(I) <= a_input(mult_width_N-1 downto 0);
      mult_b_in_b(I) <= b_input(mult_width_M-1 downto 0);
    end generate g_input_zero;
    g_input : if I > 0 generate
      mult_b_in_a(I) <= mult_N_zeros;
      mult_b_in_b(I) <= mult_M_zeros;
    end generate g_input;

    mult_b_inst : entity xil_defaultlib.mult_17x26
      port map(
        clk => sys_clk,
        a   => mult_b_in_a(I),
        b   => mult_b_in_b(I),
        p   => shifter_b_in((I+1)*(mult_width_N+mult_width_M)-1 downto I*(mult_width_N+mult_width_M))
        );
  end generate g_mult_b;


  k_shifter_a : entity ecc.k_shifter
    generic map(
      width_in         => mult_out_width,
      width_out        => width,
      shift_count      => 13,
      log2_shift_count => 4,
      k                => mult_width_N)
    port map(
      clk      => sys_clk,
      din      => shifter_a_in,
      shift_in => shift_steps_a,
      output   => shifter_dout_a
      );

  k_shifter_b : entity ecc.k_shifter
    generic map(
      width_in         => mult_out_width,
      width_out        => width,
      shift_count      => 13,
      log2_shift_count => 4,
      k                => mult_width_M)
    port map(
      clk      => sys_clk,
      din      => shifter_b_in,
      shift_in => shift_steps_b,
      output   => shifter_dout_b
      );

  column_regs_a : entity ecc.column_regs
    generic map(
      rloc_set_name  => adder_name,
      rloc_y_start   => 0,
      rloc_x_start   => -2,
      regs_per_slice => regs_per_slice_count,
      width          => width)
    port map(
      clk   => sys_clk,
      p_vec => shifter_dout_a,
      --p_vec => meme_a_dout,
      q_vec => adder_in_a
      );

  column_regs_b : entity ecc.column_regs
    generic map(
      rloc_set_name  => adder_name,
      rloc_y_start   => 0,
      rloc_x_start   => -1,
      regs_per_slice => regs_per_slice_count,
      width          => width)
    port map(
      clk   => sys_clk,
      p_vec => shifter_dout_b,
      --p_vec => meme_b_dout,
      q_vec => adder_in_b
      );


  wfc_adder_inst : entity ecc.wfc_adder
    generic map(
      adder_name => adder_name,
      width      => width,
      K_div2     => K_div2)
    port map(
      clk    => sys_clk,
      clear  => '0',
      a      => adder_in_a,
      b      => adder_in_b,
      c_in   => '0',
      output => adder_out,
      c_out  => open
      );

  column_regs_acc : entity ecc.column_regs
    generic map(
      rloc_set_name  => acc_name,
      rloc_y_start   => 0,
      rloc_x_start   => -1,
      regs_per_slice => regs_per_slice_count,
      width          => width)
    port map(
      clk   => sys_clk,
      p_vec => adder_out,
      q_vec => adder_in_acc
      );

  wfc_acc_inst : entity ecc.wfc_acc
    generic map(
      acc_name => acc_name,
      width    => width,
      K_div2   => K_div2)
    port map(
      clk    => sys_clk,
      clear  => clear,
      din    => adder_in_acc,
      output => dout(width-1 downto 0)
      );


end Behavioral;
