----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/28/2022 09:30:11 PM
-- Design Name: 
-- Module Name: ecc_pkg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Package Declaration Section
package ecc_pkg is

  constant mult_width_N   : integer := 17;
  constant mult_width_M   : integer := 26;
  constant word_count_N   : integer := 16;
  constant word_count_M   : integer := 10;
  constant mult_out_width : integer := 430;

  --Convinient constants
  constant mult_N_zeros : std_logic_vector(mult_width_N-1 downto 0) := (others => '0');
  constant mult_M_zeros : std_logic_vector(mult_width_M-1 downto 0) := (others => '0');

  -- FPGA architecture dependent
  constant LUT6_2_per_slice_count : integer := 8;
  constant regs_per_slice_count   : integer := 16;

  --Types
  type mult_N_array_t is array (0 to word_count_M-1) of
    std_logic_vector(mult_width_N-1 downto 0);
  type mult_M_array_t is array (0 to word_count_M-1) of
    std_logic_vector(mult_width_M-1 downto 0);

end package ecc_pkg;

-- Package Body Section
package body ecc_pkg is


end package body ecc_pkg;
