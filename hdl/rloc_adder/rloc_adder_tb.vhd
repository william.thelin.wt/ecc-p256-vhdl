----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/21/2022 09:16:14 PM
-- Design Name: 
-- Module Name: rloc_adder_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library ecc;
use ecc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rloc_adder_tb is
--  Port ( );
end rloc_adder_tb;

architecture Behavioral of rloc_adder_tb is

    constant period : time := 10 ns;
    constant width : integer := 48;

    signal sys_clk : std_logic := '0';
    
    signal adder_comb   : std_logic_vector(2*width-1 downto 0) := X"FFFF_FFFF_FFFE" & X"FFFF_FFFF_FFF0";
    signal adder_in_a   : std_logic_vector(width-1 downto 0) := (others => '0');
    signal adder_in_b   : std_logic_vector(width-1 downto 0) := (others => '0');
    signal adder_in_a_d1   : std_logic_vector(width-1 downto 0) := (others => '0');
    signal adder_in_b_d1   : std_logic_vector(width-1 downto 0) := (others => '0');
    signal adder_out    : std_logic_vector(width-1 downto 0) := (others => '0');
    
    signal adder_validate : std_logic_vector(width-1 downto 0) := (others => '0');

begin

    sys_clk <= not sys_clk after period/2;
    
    adder_in_a <= adder_comb(width-1 downto 0);
    adder_in_b <= adder_comb(2*width-1 downto width);
    
    rloc_adder_inst : entity ecc.rloc_adder
        generic map(
                adder_name => "tb_1",
                width => width
                )
        port map(
             clk => sys_clk,
             a => adder_in_a,
             b => adder_in_b,
             output => adder_out
             );
                   
    p_adder_input : process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            adder_in_a_d1 <= adder_in_a;
            adder_in_b_d1 <= adder_in_b;
            adder_comb <= std_logic_vector(unsigned(adder_comb) + 1);
            adder_validate <= std_logic_vector(unsigned(adder_in_a) + unsigned(adder_in_b));

		--Validate output, concat 0 with adder_validate to get positive integer
            assert unsigned(adder_validate) = unsigned(adder_out) report "Arithmetic error! Expected: "
                & integer'image(to_integer(unsigned('0' & adder_out)))
                & " got: "
                & integer'image(to_integer(unsigned('0' & adder_validate))) severity ERROR;
        end if;
    end process p_adder_input;
    
    

end Behavioral;
