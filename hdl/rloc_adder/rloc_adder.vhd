----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/20/2022 10:15:50 PM
-- Design Name: 
-- Module Name: wfc_adder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Wide Fast-Carry adder - WFC-adder
--              Generates the carry signal for wide operands faster architecture
--              ordinary.
--              K_div2 is the number of cc modules for K_div2 > 1
--              width = 8*L, L is an integer

-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

library ecc;
use ecc.all;

entity rloc_adder is
    generic(
        adder_name  : string;
        width       : integer range 8 to 2048 := 128
    );
    port(
        clk     : in std_logic;
        clear   : in std_logic;
        a       : in std_logic_vector(width-1 downto 0);
        b       : in std_logic_vector(width-1 downto 0);
        output  : out std_logic_vector(width-1 downto 0) := (others => '0')
        );
end rloc_adder;

architecture Behavioral of rloc_adder is

    constant number_of_LUT6_2_per_slice : integer := 8;
    constant number_of_fa               : integer := width;
    constant number_of_carry8_fa        : integer := number_of_fa/8;
    
    signal p_fa_vec    : std_logic_vector(number_of_fa-1 downto 0);
    signal g_fa_vec    : std_logic_vector(number_of_fa-1 downto 0);
    signal c_fa_int    : std_logic_vector(number_of_fa-1 downto 0);
    signal s_fa        : std_logic_vector(number_of_fa-1 downto 0);
    signal s_fa_d1     : std_logic_vector(number_of_fa-1 downto 0);

    
    
begin
    
    output <= s_fa_d1;

----------------------------------------------------------------------------------
-- FA
----------------------------------------------------------------------------------
    
    g_fa : for k in 0 to number_of_fa-1 generate
        fa : entity ecc.fa_lut
        generic map(
            set_name => adder_name & "_rloc_adder_set",
            rloc_index => k/number_of_LUT6_2_per_slice
            )
        port map(
            a   => a(k),
            b   => b(k),
            p   => p_fa_vec(k),
            g   => g_fa_vec(k)
            );
    end generate g_fa;
    
    g_carry_fa : for k in 0 to number_of_carry8_fa-1 generate
        g_carry_fa_g : if k > 0 generate
            CARRY8 : entity ecc.carry8_wrapper
            generic map (
                set_name => adder_name & "_rloc_adder_set",
                rloc_index => k
                )
            port map (    
                c_out => c_fa_int(8*k+7 downto 8*k),  -- 8-bit output: Carry-out
                c_xor_out => s_fa(8*k+7 downto 8*k),  -- 8-bit output: Carry chain XOR data out
                c_in_l => c_fa_int(8*k-1),            -- 1-bit input: Lower Carry-In
                c_in_m => '0',                          -- 1-bit input: Upper Carry-In
                data_in => g_fa_vec(8*k+7 downto 8*k),     -- 8-bit input: Carry-MUX data in
                sel => p_fa_vec(8*k+7 downto 8*k)          -- 8-bit input: Carry-mux select
                );
         end generate g_carry_fa_g;
         
         g_carry_fa_0 : if k = 0 generate
            CARRY8 : entity ecc.carry8_wrapper
            generic map (
                set_name => adder_name & "_rloc_adder_set",
                rloc_index => k
                )
            port map (    
                c_out => c_fa_int(7 downto 0),  -- 8-bit output: Carry-out
                c_xor_out => s_fa(7 downto 0),  -- 8-bit output: Carry chain XOR data out
                c_in_l => '0',            		-- 1-bit input: Lower Carry-In
                c_in_m => '0',                          -- 1-bit input: Upper Carry-In
                data_in => g_fa_vec(7 downto 0),     -- 8-bit input: Carry-MUX data in
                sel => p_fa_vec(7 downto 0)          -- 8-bit input: Carry-mux select
                );
         end generate g_carry_fa_0;
    end generate g_carry_fa;
    
    -- There are 16 regs/CLB, but only 8 can be used due to only 8 LUT6/CLB
    g_fa_reg : for k in 0 to number_of_fa-1 generate
        fa : entity ecc.fa_reg
        generic map(
            set_name => adder_name & "_rloc_adder_set",
            rloc_index => k/number_of_LUT6_2_per_slice
            )
        port map(
            clk   => clk,
            clear => clear,
            p   => s_fa(k),
            q   => s_fa_d1(k)
            );
    end generate g_fa_reg;
     

end Behavioral;
