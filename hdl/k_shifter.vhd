
-- Description
-- Shifts in steps of k

-- Requirement
-- width_out > width_in

library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity k_shifter is
  generic(width_in         : integer := 128;
          width_out        : integer := 256;
          shift_count      : integer := 4;
          log2_shift_count : integer := 2;
          k                : integer := 1
          );
  port(
    clk      : in  std_logic;
    din      : in  std_logic_vector(width_in-1 downto 0);
    shift_in : in  std_logic_vector(log2_shift_count-1 downto 0) := (others => '0');
    output   : out std_logic_vector(width_out-1 downto 0)        := (others => '0')
    );
end k_shifter;

architecture Behavioral of k_shifter is

  -- Convenient when concatenating
  constant zeros      : std_logic_vector(width_out-1 downto 0) := (others => '0');
  constant width_diff : integer                                := width_out - width_in;
  constant shift_lim  : integer                                := width_diff / k;

  type shift_array_t is array(0 to shift_count-1)
    of std_logic_vector(width_out-1 downto 0);
  signal shift_array : shift_array_t := (others => (others => '0'));

  --type mux_t is signal
  signal mux_out : std_logic_vector(width_out-1 downto 0);


begin

  shift_array(0) <= zeros(width_out-1 downto width_in) & din;
  --shift_array(shift_count-1) <= din & zeros(width_out-1 downto width_in);
  g_shift : for I in 1 to shift_count-1 generate
    g_no_overflow : if (shift_count <= shift_lim) generate
      shift_array(I) <= zeros(width_out-1 downto width_in + (I*k)) & din & zeros((I*k)-1 downto 0);
    end generate g_no_overflow;
    g_overflow : if (shift_count > shift_lim) generate
      g_below_lim : if I <= shift_lim generate
        shift_array(I) <= zeros(width_out-1 downto width_in + (I*k)) & din & zeros((I*k)-1 downto 0);
      end generate g_below_lim;
      g_above_lime : if I > shift_lim generate
        --T = width_in+(I*k)
        shift_array(I) <= din(width_in-(width_in+(I*k)-width_out)-1 downto 0)
                          & zeros((I*k)-1 downto 0);
      end generate g_above_lime;
    end generate g_overflow;

  -- if(width_in + I*k <= width_out) generate
  --   shift_array(I) <= zeros(width_out-1 downto width_in + (I*k)) & din & zeros((I*k)-1 downto 0);
  -- end generate;
  -- if(width_in + I*k > width_out) generate
  --   shift_array(I) <= din(width_in-(width_in + I*k - width_out)-1 downto 0)
  --                     & zeros(width_out-(width_in-1-(width_in + I*k - width_out))-1 downto 0);
  -- end generate;
  end generate g_shift;


  -- with shift_in select
  --   mux_out <=
  --   zeros(width_out-1 downto width_in) & din                                   when "00",
  --   zeros(width_out-1 downto width_in + (1*k)) & din & zeros((1*k)-1 downto 0) when "01",
  --   zeros(width_out-1 downto width_in + (2*k)) & din & zeros((2*k)-1 downto 0) when "10",
  --   zeros(width_out-1 downto width_in + (3*k)) & din & zeros((3*k)-1 downto 0) when others;

  p_ff : process(clk)
  begin
    if rising_edge(clk) then
      output <= shift_array(to_integer(unsigned(shift_in)));
    end if;

  end process p_ff;

end Behavioral;
