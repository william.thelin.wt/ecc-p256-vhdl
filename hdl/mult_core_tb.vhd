----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/29/2022 08:36:50 PM
-- Design Name: 
-- Module Name: mult_core_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library ecc;
use ecc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mult_core_tb is
--  Port ( );
end mult_core_tb;

architecture Behavioral of mult_core_tb is

    constant period : time := 10 ns;
    constant width : integer := 512;
    constant mult_width : integer := 256;
    constant K_div2 : integer := 8*26;

    signal sys_clk : std_logic := '0';


    signal a_input      : std_logic_vector(mult_width-1 downto 0) := (others => '0');
    signal b_input      : std_logic_vector(mult_width-1 downto 0) := (others => '0');
    signal din_vld      : std_logic := '0';
    signal din_vld_d1   : std_logic := '0';
    signal mult_rdy         : std_logic;
    signal mult_out_vld     : std_logic;
    signal mult_core_out    : std_logic_vector(width-1 downto 0) := (others => '0');
    signal mult_core_out_d1 : std_logic_vector(width-1 downto 0) := (others => '0');

begin

    sys_clk <= not sys_clk after period/2;

    
    mult_core_inst : entity ecc.mult_p256_core
    generic map(
      mult_name => "my_mult"
      )
    port map(
      sys_clk  => sys_clk,
      a_input  => a_input,
      b_input  => b_input,
      din_vld  => din_vld,
      rdy_out  => mult_rdy,
      dout     => mult_core_out,
      dout_vld => mult_out_vld
      );
      
    
    -- purpose: Stimulus to mult_core
    p_stimuli: process (sys_clk)
    begin  -- process p_stimuli
      if rising_edge(sys_clk) then
        din_vld_d1 <= din_vld;
      
        if(din_vld = '1') then
            din_vld <= '0';
            a_input <= std_logic_vector(to_unsigned(0, mult_width));
            b_input <= std_logic_vector(to_unsigned(0, mult_width));
        elsif(mult_rdy  = '1' and din_vld_d1 = '0') then
            din_vld <= '1';
            a_input(mult_width-1-17 downto 0) <= std_logic_vector(to_unsigned(2, mult_width-17));
            b_input(mult_width-1-17 downto 0) <= std_logic_vector(to_unsigned(1, mult_width-17));
            a_input(mult_width-1 downto mult_width-17) <= std_logic_vector(to_unsigned(1, 17));
            b_input(mult_width-1 downto mult_width-17) <= std_logic_vector(to_unsigned(1, 17));
        else
            din_vld <= '0';
            a_input <= std_logic_vector(to_unsigned(0, mult_width));
            b_input <= std_logic_vector(to_unsigned(0, mult_width));
        end if;
      end if;
    end process p_stimuli;
    
    p_validate : process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            mult_core_out_d1 <= mult_core_out;
            --Validate output, concat 0 with adder_validate to get positive integer
            -- assert unsigned(adder_validate) = unsigned(adder_out) report "Arithmetic error! Expected: "
            --     & integer'image(to_integer(unsigned('0' & adder_out)))
            --     & " got: "
            --     & integer'image(to_integer(unsigned('0' & adder_validate_d1))) severity ERROR;
        end if;
    end process p_validate;

end Behavioral;
