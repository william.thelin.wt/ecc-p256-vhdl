----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/20/2022 10:51:56 PM
-- Design Name: 
-- Module Name: g_adder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Adder with generic width
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity g_adder is
    generic(width : integer := 128);
    port(
        clk     : in std_logic;
        a       : in std_logic_vector(width-1 downto 0);
        b       : in std_logic_vector(width-1 downto 0);
        output  : out std_logic_vector(width-1 downto 0)
        );
end g_adder;

architecture Behavioral of g_adder is

begin

    p_add : process(clk)
    begin
        if rising_edge(clk) then
            output <= std_logic_vector(unsigned(a) + unsigned(b));
        end if;
    
   end process p_add;     

end Behavioral;
