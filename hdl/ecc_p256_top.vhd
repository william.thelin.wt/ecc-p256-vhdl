----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/19/2022 04:20:32 PM
-- Design Name: 
-- Module Name: ecc_p256_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

use IEEE.NUMERIC_STD.all;

library ecc;
use ecc.all;

use ecc.ecc_pkg.all;

library xil_defaultlib;
use xil_defaultlib.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ecc_p256_top is
  port(
    --system clocks from Si5394J
    SYSCLK2_N : in std_logic;
    SYSCLK2_P : in std_logic;
    SYSCLK3_N : in std_logic;
    SYSCLK3_P : in std_logic;

    --PCIE clocks from Si5394J
    PCIE_REFCLK0_N : in std_logic;
    PCIE_REFCLK0_P : in std_logic;
    PCIE_SYSCLK0_N : in std_logic;
    PCIE_SYSCLK0_P : in std_logic;

    PCIE_REFCLK1_N : in std_logic;
    PCIE_REFCLK1_P : in std_logic;
    PCIE_SYSCLK1_N : in std_logic;
    PCIE_SYSCLK1_P : in std_logic;

    --QSFP clocks from 
    SYNCE_CLK0_N : in std_logic;
    SYNCE_CLK0_P : in std_logic;
    SYNCE_CLK1_N : in std_logic;
    SYNCE_CLK1_P : in std_logic
    );
end ecc_p256_top;

architecture Behavioral of ecc_p256_top is

  constant mult_width : integer := 256;
  constant width      : integer := 2*mult_width;
  constant K_div2     : integer := 8*24;


  --System clock
  signal sys_clk        : std_logic;
  signal sys_clk_locked : std_logic := '0';

  signal sys_reset : std_logic := '0';


  signal mem_a_addr    : unsigned(0 downto 0)                    := (others => '0');
  signal mem_b_addr    : unsigned(0 downto 0)                    := (others => '0');
  signal mem_comb_addr : unsigned(1 downto 0)                    := (others => '0');
  signal mem_a_dout    : std_logic_vector(mult_width-1 downto 0) := (others => '0');
  signal mem_b_dout    : std_logic_vector(mult_width-1 downto 0) := (others => '0');

  signal mult_rdy         : std_logic;
  signal mult_out_vld     : std_logic;
  signal mult_core_out    : std_logic_vector(width-1 downto 0) := (others => '0');
  signal mult_core_out_d1 : std_logic_vector(width-1 downto 0) := (others => '0');



  attribute dont_touch                     : boolean;
  --attribute dont_touch of clear         : signal is true;
  --attribute dont_touch of acc_out_d1    : signal is true;
  --attribute dont_touch of acc_out       : signal is true;
  attribute dont_touch of mult_core_out_d1 : signal is true;



begin

  sys_reset <= not(sys_clk_locked);

  --Generates internal clocks from 100MHz ref clock
  system_clock_gen_inst : entity xil_defaultlib.system_clock_gen
    port map(
      clk_in1_n => SYSCLK2_N,
      clk_in1_p => SYSCLK2_P,
      clk_out1  => sys_clk,
      locked    => sys_clk_locked
      );


  mem_a_addr <= mem_comb_addr(0 downto 0);
  mem_b_addr <= mem_comb_addr(1 downto 1);

  a_mem : entity xil_defaultlib.adder_mem
    port map(
      clka   => sys_clk,
      ena    => '1',
      wea(0) => '0',
      addra  => std_logic_vector(mem_a_addr),
      dina   => (others => '0'),
      douta  => mem_a_dout
      );

  b_mem : entity xil_defaultlib.adder_mem
    port map(
      clka   => sys_clk,
      ena    => '1',
      wea(0) => '0',
      addra  => std_logic_vector(mem_b_addr),
      dina   => (others => '0'),
      douta  => mem_b_dout
      );

  mult_core_inst : entity ecc.mult_p256_core
    generic map(
      mult_name => "my_mult"
      )
    port map(
      sys_clk  => sys_clk,
      a_input  => mem_a_dout,
      b_input  => mem_b_dout,
      din_vld  => '1',
      rdy_out  => mult_rdy,
      dout     => mult_core_out,
      dout_vld => mult_out_vld
      );



  p_mult_core : process(sys_clk)
  begin
    if rising_edge(sys_clk) then
      mem_comb_addr    <= mem_comb_addr + 1;
      mult_core_out_d1 <= mult_core_out;
    end if;
  end process p_mult_core;


end Behavioral;
