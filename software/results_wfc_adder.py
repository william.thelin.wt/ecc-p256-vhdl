
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np


x = np.linspace(16, 256, 256-16)  # Sample data.

label_256 = 'N = 256'
width_256 = 256
target_cp_256 = 1.6
cp_256 = np.array([
	[8*3, -0.377], \
	[8*4, -0.369], \
	[8*5, -0.301], \
	[8*6, -0.193], \
	[8*7, -0.198], \
	[8*8, -0.187], \
	[8*9, -0.195], \
	[8*10, -0.200], \
	[8*11, -0.164], \
	[8*12, -0.431], \
	[8*13, -0.223], \
	[8*14, -0.360], \
	[8*15, -0.386] \
	])
abs_cp_256 = cp_256
abs_cp_256[:,1] -= target_cp_256
abs_cp_256[:,1] *= -1

data_256 = abs_cp_256
data_256[:,0] = width_256 - 2*data_256[:,0]

	
label_512 = 'N = 512'
width_512 = 512
target_cp_512 = 2.0
cp_512 = np.array([
	[8*16, -0.740], \
	[8*18, -0.551], \
	[8*20, -0.361], \
	[8*22, -0.341], \
	[8*23, -0.305], \
	[8*24, -0.256], \
	[8*25, -0.263], \
	[8*26, -0.165], \
	[8*27, -0.244], \
	[8*28, -0.310], \
	[8*29, -0.391], \
	[8*30, -0.232], \
	[8*31, -0.396], \
	])
abs_cp_512 = cp_512
abs_cp_512[:,1] -= target_cp_512
abs_cp_512[:,1] *= -1

data_512 = abs_cp_512
data_512[:,0] = width_512 - 2*data_512[:,0]

# Note that even in the OO-style, we use `.pyplot.figure` to create the Figure.
fig, ax = plt.subplots()
ax.plot(data_256[:,0], data_256[:,1], label=label_256)  # Plot some data on the axes.
ax.plot(data_512[:,0], data_512[:,1], label=label_512)  # Plot more data on the axes...

ax.set_xlabel('M [bits]')  # Add an x-label to the axes.
ax.set_ylabel('Critical path [ns]')  # Add a y-label to the axes.
#ax.set_title("")  # Add a title to the axes.
ax.legend();  # Add a legend.

plt.savefig('wfc_critical_path')

