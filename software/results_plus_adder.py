
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np


x = np.linspace(16, 256, 256-16)  # Sample data.

label_adder = 'VHDL +'
data = np.array([
	[128, 700], \
	[192, 580], \
	[256, 480], \
	[384, 366], \
	[430, 333]
	])


# Note that even in the OO-style, we use `.pyplot.figure` to create the Figure.
fig, ax = plt.subplots()
ax.plot(data[:,0], data[:,1], label=label_adder)  # Plot some data on the axes.

ax.set_xlabel('Bitwidth [bits]')  # Add an x-label to the axes.
ax.set_ylabel('Frequency [MHz]')  # Add a y-label to the axes.
#ax.set_title("")  # Add a title to the axes.
ax.legend();  # Add a legend.

plt.savefig('results_plus_adder')

