\documentclass[]{article}

%Packages
\usepackage{booktabs} % Nice table lines
\newcommand{\otoprule}{\midrule[\heavyrulewidth]} % Fix extra line

\usepackage{parskip} % Space between paragraphs

\usepackage{tikz} % Drawing figures
\usetikzlibrary{intersections} % For making tidy math calcs in tikz pic

\usepackage{cite} % Good handling of citations

\usepackage{amsmath} % \text{} in math mode
\usepackage{amsfonts} % Nice capital letters for sets

\usepackage{multirow}

%opening
\title{Theory regarding elliptic curve cryptography}
\author{William Thelin}

\begin{document}

\maketitle


\section{Introduction}
This document serves as a knowledge base during my participation in the Adaptive Compute Challenge 2021, sponsored by Xilinx and hosted by Hackster.io.

\section{Number Systems}
Often, the decimal number system is the only number system one gets in contact with in the ordinary academic universe. Although, there exists an infinite number of number systems with different bases. The decimal number system has a base of 10 (sometimes called denary or decanary). The second most popular number is the binary (base 2) number system. There is also the ternary (base 3) number system and so on.

In Table \ref{tab:cmp_number_systems} is a comparison of the integer representation in the decimal, binary and ternary number systems.

\begin{table}[tbp]
	\centering
	\caption{\label{tab:cmp_number_systems}The integers from 0 to 10 represented in different number systems.}
	\begin{tabular}{l|l|l}
		\toprule%
		Decimal & Binary & Ternary\\\otoprule%
		00 & 0000 & 000\\
		01 & 0001 & 001\\
		02 & 0010 & 002\\
		03 & 0011 & 010\\
		04 & 0100 & 011\\
		05 & 0101 & 012\\
		06 & 0110 & 020\\
		07 & 0111 & 021\\
		08 & 1000 & 022\\
		09 & 1001 & 100\\
		10 & 1010 & 101\\
		\bottomrule
	\end{tabular}
\end{table}

\subsection{Multi-Base Number Systems}
Number systems using multiple bases. For example {$p$,$q$} = {2,3}, observe that $p$ and $q$ are chosen as relatively prime or coprime.

\subsection{Double-Base Number System - DBNS}
Given $p, q$, two relatively prime positive integers, the DBNS is a representation scheme into which every positive integer $n$ is represented as the sum or difference of {$p, q$}-integers, i.e., numbers of the form $p^a q^b$:
\[ n = \sum_{i}^{j} s_i p^a q^b \]

\section{Coordinate Systems}

\subsection{Affine Coordinate}
Euclidean spaces (including the one-dimensional line, two-dimensional plane, and three-dimensional space commonly studied in elementary geometry, as well as higher-dimensional analogues) are affine spaces. 

\subsection{Jacobian Projective Coordinate}
Jacobian Coordinates are used to represent elliptic curve points on prime curves $y^2 = x^3 + a x + b$. They give a speed benefit over Affine Coordinates when the cost for field inversions is significantly higher than field multiplications. In Jacobian Coordinates the triple $(X,Y,Z)$  represents the affine point $({\frac{X}{Z^{2}}},{\frac {Y}{Z^{3}}})$.

\section{Operations in ECC}

Operations in ECC can be viewed in a hierarchy with 4 layers, illustrated in Figure \ref{fig:ecc_hierarchy}. From the bottom up it consist of \textit{Finite Field Operations}, \textit{Elliptic Curve Addition and Doubling}, \textit{Point Multiplication} and \textit{Application/Protocol}. The operations in a layer depends on the operations in the layer below it. For example the ECDSA protocol, associated with the top layer, is implemented using curve point multiplications, associated with the layer below it.

\begin{figure}
	\centering
	
	\begin{tikzpicture}
		
		\def\hOne{Finite Field Arithmetic}
		\def\hTwo{Elliptic Curve Addition and Doubling}
		\def\hThree{Point Multiplication}
		\def\hFour{Application/Protocol}
		
		\coordinate (A) at (-4,0) {};
		\coordinate (B) at ( 4,0) {};
		\coordinate (C) at (0,6) {};
		\draw[name path=AC] (A) -- (C);
		\draw[name path=BC] (B) -- (C);
		\foreach \y/\A in {0/\hOne,1/\hTwo,2/\hThree,3/\hFour} {
			\path[name path=horiz] (A|-0,\y) -- (B|-0,\y);
			\draw[name intersections={of=AC and horiz,by=P},
			name intersections={of=BC and horiz,by=Q}] (P) -- (Q)
			node[midway,above] {\A};
		}
	\end{tikzpicture}

	\label{fig:ecc_hierarchy}
	\caption{Hierarchy of operations in ECC.}
\end{figure}

\section{Notes to self}
\begin{itemize}
	\item Montgomery Multiplication is an algorithm to avoid division in modular multiplication. Very efficient for FPGA implementations.
	\item The Jacobian Coordinates are used to avoid the divisions in the algorithm that computes curve doubling and curve addition.
\end{itemize}


\iffalse
\section{DFT in a Finite Field}
The following piece of text is found in (CITE HÄR! J.M. Pollard).

Let $GF(p^n)$, or \textit{F} for short, denote the Galois Field (Finite Field) of $p^n$ elements, where $p$ is a prime and $n$ a positive integer. Let $d$ be a divisor of $p^n - 1$ (possibly $p^n - 1$), and $r$ be a member of \textit{F} of order $d$ in the multiplicative group, \textit{F}$\star$ say, of the nonzero elements of \textit{F}. Then on can define the transform of a sequence ($a_i$) ($0 \leq i \leq d-1$) of members of \textit{F} to be the sequence ($A_i$) where
\begin{equation}
	\label{eq:DFT}
	A_i = \sum_{i=0}^{d-1} a_i r^{ij}.
\end{equation}
The transformed sequence ($A_i$) depends on the choice of $r$.

The inverse transform to (\ref{eq:DFT}) is
\begin{equation}
	\label{eq:IDFT}
	a_i = -d' \sum_{i=0}^{d-1} A_i r^{-ij},
\end{equation}
where $d'$ is the integer for which
\begin{equation}
	d'd = p^n - 1.
\end{equation}

The transform (\ref{eq:DFT}) and its inverse (\ref{eq:IDFT}) may be calculate by the FFT algorithm. The FFT algorithm is primarily performed on a sequence of complex numbers. However, if the roots of unity,
\[
	e(k/d) = e^{2\pi i k/d}, \; \; (k \text{ an integer}),
\] in the original fourier transform are replaced by $r^k$, and the operations of complex addition and multiplication become the corresponding operations in \text{F}. Then the same FFT algorithm can be utilized for (\ref{eq:DFT}).

\fi

\section{Results}
Results from synthesis of VHDL  $ + $-operator.


\iffalse
\begin{table}[tbp]
	\centering
	\caption{\label{tab:adder_comp} Delay and resource usage for a 256-bit WFC-adder.}
	\begin{tabular}{c|c|c}
		%\toprule%
		 & \multicolumn{2}{c}{\multirow{2}{*}{\textbf{REF}}}\\
		 & \multicolumn{2}{c}{}\\
		 & \multicolumn{2}{c}{Virtex Ultrascale+}\\\hline
		 & $T$ & $A$ \\
		K/2 & ns & LUT \\\hline
		16 & 2.531 & 425\\
		24 & 3.900 & 413\\
		32 & 2.613 & 401\\
		40 & 2.527 & 389\\
		48 & 2.499 & 377\\
		56 & 2.495 & 256\\
		64 & 2.420 & 256\\
		80 & 2.259 & 256\\
		96 & 2.171 & 256\\
		104 & 2.089 & 256\\
		112 & 2.020 & 256\\
		\bottomrule
	\end{tabular}
\end{table}
\fi

\begin{table}[tbp]
	\centering
	\caption{\label{tab:512adder_comp} Delay and resource usage for a 512-bit WFC-adder.}
	\begin{tabular}{c|c|c}
		%\toprule%
		& \multicolumn{2}{c}{\multirow{2}{*}{\textbf{REF}}}\\
		& \multicolumn{2}{c}{}\\
		& \multicolumn{2}{c}{Virtex Ultrascale+}\\\hline
		& $T$ & $A$ \\
		K/2 & ns & LUT \\\hline
		8*16 & 2.740 & 512\\
		8*18 & 2.551 & 512\\
		8*20 & 2.361 & 512\\
		8*22 & 2.341 & 512\\
		8*23 & 2.305 & 512\\
		8*24 & 2.256 & 512\\
		8*25 & 2.263 & 512\\
		8*26 & 2.165 & 512\\
		8*27 & 2.244 & 512\\
		8*28 & 2.310 & 512\\
		8*29 & 2.391 & 512\\
		8*30 & 2.232 & 512\\
		8*31 & 2.396 & 512\\
		\bottomrule
	\end{tabular}
\end{table}


\end{document}
